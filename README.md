# README #

### Project Name ###
Dungeon Crawler

### Group Members ###
* Aditya Subramanian (subramaniana@carleton.edu)
* Charles Eyermann (eyermannc@carleton.edu)
* Thomas Redding (reddingt@carleton.edu)

### Instructions ###

1. Git clone the project
2. Open the project in IntelliJ IDEA 14
3. Compile the project normally
4. Run main() in the class "Main"
5. Read the provided instructions on the start screen