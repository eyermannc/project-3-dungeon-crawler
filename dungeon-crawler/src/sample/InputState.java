package sample;

/**
 * Created by thomasredding on 5/24/15.
 */
public class InputState {
    public boolean up = false; // is up currently pressed
    public boolean down = false; // is down currently pressed
    public boolean left = false; // is left currently pressed
    public boolean right = false; // is right currently pressed

    public boolean initalUp = false; // was up pressed since last frame
    public boolean initalDown = false; // was down pressed since last frame
    public boolean initalLeft = false; // was left pressed since last frame
    public boolean initalRight = false; // was right pressed since last frame
    public boolean initalOpenDoor = false;
    public boolean initalAttack = false;
}
