package sample;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.animation.AnimationTimer;
import javafx.scene.input.KeyEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class Main extends Application {
    Canvas canvas;
    private Model model;
    private View view;
    private Controller controller;
    long timeOfLastDrawFrame = 0;
    long drawFrameCounter = 0;
    boolean isPaused = true;

    AnimationTimer timer = new AnimationTimer() {
        @Override
        public void handle(long now) {
            if(isPaused) {
                drawMenu();
            }
            else {
                long currentTime = System.currentTimeMillis();
                if(currentTime - timeOfLastDrawFrame > 100) {
                    timeOfLastDrawFrame = currentTime;

                    if(drawFrameCounter%2 == 0) {
                        model.think();
                    }
                    view.draw();
                    drawFrameCounter++;
                }
            }
        }
    };

    private void drawMenu() {
        GraphicsContext c = canvas.getGraphicsContext2D();
        c.clearRect(0, 0, 800, 600);
        c.setFill(Color.BLACK);
        c.setFont(Font.font(50));
        c.fillText("P - pause (leave this screen)", 100, 100);
        c.fillText("J - attack", 100, 160);
        c.fillText("K - open door", 100, 220);

        c.fillText("W - move up", 100, 340);
        c.fillText("A - move left", 100, 400);
        c.fillText("S - move down", 100, 460);
        c.fillText("D - move right", 100, 520);

    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        Group root = new Group();
        canvas = new Canvas(800, 600);
        root.getChildren().add(canvas);
        FXMLLoader loader=new FXMLLoader(getClass().getResource("basics.fxml"));
        model = new Model();
        model.loadNewLevel();
        view = new View(canvas);
        controller = new Controller();
        view.setModel(model);
        controller.setModel(model);

        primaryStage.setResizable(false);
        primaryStage.setTitle("Dungeon Crawler");
        Scene scene = new Scene(root, 800, 600);

        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if(keyEvent.getCode() == KeyCode.P) {
                    isPaused = !isPaused;
                }
                else {
                    controller.keyDown(keyEvent);
                }
            }
        });

        scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                controller.keyUp(keyEvent);
            }
        });

        primaryStage.setScene(scene);
        primaryStage.show();
        timer.start();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
