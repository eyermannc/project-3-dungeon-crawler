package sample;

/**
 * Created by thomasredding on 5/29/15.
 */
public class Goblin extends Enemy {
    public Goblin(Model model) {
        super(model);
        x = 32;
        y = 34;
        type = 0;
    }
}
