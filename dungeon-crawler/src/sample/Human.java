package sample;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by subramaniana on 5/27/15.
 */
//We unofficially named  him Jadrian
public class Human extends Creature {
    List<Item> items = new ArrayList<Item>();
    public Human(Model model) {
        super(model);
        x = 3;
        y = 3;
    }

    int timeSinceLastDamaged = 0;

    public void die() {
        health = 100;
        x = 3;
        y = 3;
        modelref.score -= 400;
        if(modelref.score < 0) {
            modelref.score = 0;
        }
    }

    public void think() {
        //if you're standing on an item, pick it up
        for(int i = 0; i < modelref.currentFloor.items.size(); i++) {
            Item item = modelref.currentFloor.items.get(i);
            if(item.xLocation == x && item.yLocation == y) {
                items.add(item);
                modelref.currentFloor.items.remove(i);
                break;
            }
        }
        if(health < 0) {
            die();
        }
        else if(health < 100) {
            // slowly heal
            if(timeSinceLastDamaged > 10) {
                health+=5;
            }
            else {
                health++;
            }
            if(health > 100) {
                health = 100;
            }
        }
        if(modelref.currentFloor.tiles[x][y] == 6) {
            // we win
            modelref.loadNewLevel();
        }
        timeSinceLastDamaged++;
    }

    public void handleInput(InputState input) {
        int deltaY = 0;
        int deltaX = 0;
        if(input.up || input.initalUp) {
            deltaY--;
            input.initalUp = false;
        }
        if(input.down || input.initalDown) {
            deltaY++;
            input.initalDown = false;
        }
        if(input.left || input.initalLeft) {
            deltaX--;
            input.initalLeft = false;
        }
        if(input.right || input.initalRight) {
            deltaX++;
            input.initalRight = false;
        }

        boolean didDeltaXSucceed = true;
        boolean didDeltaYSucceed = true;

        if(deltaX > 0) {
            didDeltaXSucceed  = moveRight();
        }
        else if(deltaX < 0) {
            didDeltaXSucceed  = moveLeft();
        }
        if(deltaY > 0) {
            didDeltaYSucceed = moveDown();
        }
        else if(deltaY < 0) {
            didDeltaYSucceed = moveUp();
        }

        // move nicely around corners
        if(didDeltaYSucceed && !didDeltaXSucceed) {
            if(deltaX > 0) {
                moveRight();
            }
            else if(deltaX < 0) {
                moveLeft();
            }
        }
        else if(!didDeltaYSucceed && didDeltaXSucceed) {
            if(deltaY > 0) {
                moveDown();
            }
            else if(deltaY < 0) {
                moveUp();
            }
        }

        // open doors
        if(input.initalAttack) {
            input.initalAttack = false;
            attack();
        }
        if(input.initalOpenDoor) {
            input.initalOpenDoor = false;
            openDoor();
        }

    }

    public void attack() {
        isAttacking = true;
        if(orientation == 0) {
            modelref.currentFloor.hitboxes[x][y-1] = 30;
        }
        else if(orientation == 1) {
            modelref.currentFloor.hitboxes[x+1][y] = 30;
        }
        else if(orientation == 2) {
            modelref.currentFloor.hitboxes[x][y+1] = 30;
        }
        else if(orientation == 3) {
            modelref.currentFloor.hitboxes[x-1][y] = 30;
        }
    }

    public void openDoor() {
        if(getOrientation() == 0) {
            if(y != 0) {
                if(modelref.currentFloor.tiles[x][y-1] == 3) {
                    modelref.currentFloor.tiles[x][y-1] = 2;
                } else if (modelref.currentFloor.tiles[x][y-1] == 4 &&
                        items.size() > 0) {
                    modelref.currentFloor.tiles[x][y-1] = 2;
                    items.remove(items.size()-1);
                }
            }
        }
        else if(getOrientation() == 1) {
            if(x < modelref.currentFloor.tiles.length-1) {
                if(modelref.currentFloor.tiles[x+1][y] == 3) {
                    modelref.currentFloor.tiles[x+1][y] = 2;
                } else if (modelref.currentFloor.tiles[x+1][y] == 4 &&
                        items.size() > 0) {
                    modelref.currentFloor.tiles[x+1][y] = 2;
                    items.remove(items.size()-1);
                }
            }
        }
        else if(getOrientation() == 2) {
            if(y < modelref.currentFloor.tiles[0].length-1) {
                if(modelref.currentFloor.tiles[x][y+1] == 3) {
                    modelref.currentFloor.tiles[x][y+1] = 2;
                } else if (modelref.currentFloor.tiles[x][y+1] == 4 &&
                        items.size() > 0) {
                    modelref.currentFloor.tiles[x][y+1] = 2;
                    items.remove(items.size()-1);
                }
            }
        }
        else if(getOrientation() == 3) {
            if(x > 0) {
                if(modelref.currentFloor.tiles[x-1][y] == 3) {
                    modelref.currentFloor.tiles[x-1][y] = 2;
                } else if (modelref.currentFloor.tiles[x-1][y] == 4 &&
                        items.size() > 0) {
                    modelref.currentFloor.tiles[x-1][y] = 2;
                    items.remove(items.size()-1);
                }
            }
        }
    }
}
