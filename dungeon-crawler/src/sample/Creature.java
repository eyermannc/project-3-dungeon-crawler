package sample;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;

/**
 * Created by thomasredding on 5/25/15.
 */
//abstract class which human and enemy is based off of
//a lot of general stuff which could be used exists here
public abstract class Creature {
    int x = 0;
    int y = 0;
    int oldX = 0;
    int oldY = 0;
    int maxHealth = 100;
    int health = 100;
    int attack = 1;
    int defense = 1;
    int orientation = 0;
    Model modelref;
    public boolean isAttacking = false;


    public Creature(Model model) {
        modelref = model;
    }

    abstract void attack();

    // for enemy, remove self from list of enemies.
    // For protagonist, cue gameOver scene
    abstract void die();

    public void lookUp() {orientation = 0;}
    public void lookRight() {orientation = 1;}
    public void lookDown() {orientation = 2;}
    public void lookLeft() {orientation = 3;}

    public void setLocation(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point2D getLocation() {
        return new Point2D((double) (this.x), (double) (this.y));
    }

    public void setHealth(int newHealth) {
        this.health = newHealth;
    }
    public void setMaxHealth(int newMax) {
        this.maxHealth= newMax;
    }
    public int getHealth() {return this.health;}
    public int getMaxHealth() {return this.maxHealth;}
    public int getAttack() {return this.attack;}
    public void setAttack(int i) {this.attack = i;}
    public void setDefense(int i) {this.defense = i;}
    public int getDefense() {return this.defense;}
    public int getOrientation() {return this.orientation;}

    public void takeDamage(int dmgTaken) {
        this.health = health - dmgTaken;
        if(health <= 0) {this.die();}
    }

    public void gainHealth(int healthGained) {
        this.health = health + healthGained;
    }

    private boolean tileHasCreature(int xPos, int yPos) {
        for(int i=0; i<modelref.currentFloor.monsters.size(); i++) {
            if(xPos == modelref.currentFloor.monsters.get(i).x) {
                if(yPos == modelref.currentFloor.monsters.get(i).y) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean moveDown() throws IndexOutOfBoundsException{
        this.lookDown();
        try {
            int tileAboveCreature = modelref.currentFloor.tiles[x][y + 1];
            if (tileAboveCreature == 0 || tileAboveCreature == 2 ||
                    tileAboveCreature == 6) {
                if(!tileHasCreature(x,  y+1)) {
                    this.y++;
                    return true;
                }
                else {return false;}
            } else {return false;}
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
        //try to move up, If possible, do so.
    }

    public boolean moveUp() throws  IndexOutOfBoundsException{
        this.lookUp();
        try {
            int tileAboveCreature = modelref.currentFloor.tiles[x][y - 1];
            if (tileAboveCreature == 0 || tileAboveCreature == 2 ||
                    tileAboveCreature == 6) {
                if(!tileHasCreature(x, y-1)) {
                    this.y--;
                    return true;
                }
                else {return false;}
            } else {return false;}} catch (IndexOutOfBoundsException e) {
            return false;
        }
        //try to move down, If possible, do so.
    }

    public boolean moveRight() throws IndexOutOfBoundsException{
        this.lookRight();
        try {
            int tileAboveCreature = modelref.currentFloor.tiles[x + 1][y];
            if (tileAboveCreature == 0 || tileAboveCreature == 2 ||
                    tileAboveCreature == 6) {
                if(!tileHasCreature(x+1, y)) {
                    this.x++;
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        catch (IndexOutOfBoundsException e) {
            return false;
        }
        //try to move right, If possible, do so.
    }

    public boolean moveLeft() throws IndexOutOfBoundsException{
        this.lookLeft();
        try {
            int tileAboveCreature = modelref.currentFloor.tiles[x-1][y];
            if (tileAboveCreature == 0 || tileAboveCreature == 2 ||
                    tileAboveCreature == 6) {
                if(!tileHasCreature(x-1, y)) {
                    this.x--;
                    return true;
                }
                else {return false;}
            } else {return false;}} catch (IndexOutOfBoundsException e) {
            return false;
        }
        //try to move left, If possible, do so.
    }

    public void draw(GraphicsContext a, double b, double c, int d) {}

    public void update() {
        oldX = x;
        oldY = y;
    }
}
