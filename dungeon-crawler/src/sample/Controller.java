package sample;

/**
 * Created by thomasredding on 5/24/15.
 */
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class Controller {
    private InputState input;
    private Model model;

    public Controller() {
        input = new InputState();
    }

    public void setModel(Model modelRef) {
        model = modelRef;
        model.setInput(input);
    }

    public void keyDown(KeyEvent event) {
        KeyCode code = event.getCode();
        if (code == KeyCode.LEFT || code == KeyCode.A) {
            if(!input.left) {
                input.initalLeft = true;
            }
            input.left = true;
            event.consume();
        }
        else if (code == KeyCode.RIGHT || code == KeyCode.D) {
            if(!input.right) {
                input.initalRight = true;
            }
            input.right = true;
            event.consume();
        }
        else if (code == KeyCode.UP || code == KeyCode.W) {
            if(!input.up) {
                input.initalUp = true;
            }
            input.up = true;
            event.consume();
        }
        else if (code == KeyCode.DOWN || code == KeyCode.S) {
            if(!input.down) {
                input.initalDown = true;
            }
            input.down = true;
            event.consume();
        }
        else if (code == KeyCode.J) {
            if(!input.initalAttack) {
                input.initalAttack= true;
            }
            event.consume();
        }
        else if (code == KeyCode.K) {
            if(!input.initalOpenDoor) {
                input.initalOpenDoor = true;
            }
            event.consume();
        }
    }

    public void keyUp(KeyEvent event) {
        KeyCode code = event.getCode();
        if (code == KeyCode.LEFT || code == KeyCode.A) {
            input.left = false;
            event.consume();
        }
        else if (code == KeyCode.RIGHT || code == KeyCode.D) {
            input.right = false;
            event.consume();
        }
        else if (code == KeyCode.UP || code == KeyCode.W) {
            input.up = false;
            event.consume();
        }
        else if (code == KeyCode.DOWN || code == KeyCode.S) {
            input.down = false;
            event.consume();
        }
    }

    public void windowResized(double width, double height) {
    }
}
