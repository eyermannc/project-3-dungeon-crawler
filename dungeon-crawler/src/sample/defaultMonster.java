package sample;

import javafx.scene.image.Image;


/**
 * Created by subramaniana on 5/31/15.
 */
//loads images so we don't have to load them every time we create a monster
public class defaultMonster {
    Image left = new Image("/WarriorEnemy/Warrior_wpn_1 " +
            "[www.imagesplitter.net]-1-0.png");
    Image leftAlt = new Image("/WarriorEnemy/Warrior_wpn_1 " +
            "[www.imagesplitter.net]-1-2.png");
    Image right = new Image("/WarriorEnemy/Warrior_wpn_1 " +
            "[www.imagesplitter.net]-2-0.png");
    Image rightAlt = new Image("/WarriorEnemy/Warrior_wpn_1 " +
            "[www.imagesplitter.net]-2-2.png");
    Image up = new Image("/WarriorEnemy/Warrior_wpn_1 " +
            "[www.imagesplitter.net]-3-0.png");
    Image upAlt = new Image("/WarriorEnemy/Warrior_wpn_1 " +
            "[www.imagesplitter.net]-3-2.png");
    Image down = new Image("/WarriorEnemy/Warrior_wpn_1 " +
            "[www.imagesplitter.net]-0-0.png");//
    Image downAlt = new Image("/WarriorEnemy/Warrior_wpn_1 " +
            "[www.imagesplitter.net]-0-1.png");
    Image attackLeft = new Image("/WarriorEnemy/Warrior_wpn_1 " +
            "[www.imagesplitter.net]-1-3.png");
    Image attackLeftAlt = new Image("/WarriorEnemy/Warrior_wpn_1 " +
            "[www.imagesplitter.net]-1-1.png");
    Image attackRightAlt = new Image("/WarriorEnemy/Warrior_wpn_1 " +
            "[www.imagesplitter.net]-2-1.png");
    Image attackRight = new Image("/WarriorEnemy/Warrior_wpn_1 " +
            "[www.imagesplitter.net]-2-3.png");
    Image attackUpAlt = new Image("/WarriorEnemy/Warrior_wpn_1 " +
            "[www.imagesplitter.net]-3-1.png");
    Image attackUp = new Image("/WarriorEnemy/Warrior_wpn_1 " +
            "[www.imagesplitter.net]-3-3.png");
    Image attackDownAlt = new Image("/WarriorEnemy/Warrior_wpn_1 " +
            "[www.imagesplitter.net]-0-3.png");
    Image attackDown = new Image("/WarriorEnemy/Warrior_wpn_1 " +
            "[www.imagesplitter.net]-0-2.png");
    public Image[] imageList = {left, leftAlt, right, rightAlt, up, upAlt, down,
            downAlt, attackLeft, attackLeftAlt,attackRight, attackRightAlt,
            attackUp, attackUpAlt, attackDown, attackDownAlt};

    Image left1 = new Image("/walk/skeleton_sprites " +
            "[www.imagesplitter.net]-1-4.png");
    Image leftAlt1 = new Image("/walk/skeleton_sprites " +
            "[www.imagesplitter.net]-1-2.png");
    Image right1 = new Image("/walk/skeleton_sprites " +
            "[www.imagesplitter.net]-3-5.png");
    Image rightAlt1 = new Image("/walk/skeleton_sprites " +
            "[www.imagesplitter.net]-3-0.png");
    Image up1 = new Image("/walk/skeleton_sprites " +
            "[www.imagesplitter.net]-0-1.png");
    Image upAlt1 = new Image("/walk/skeleton_sprites " +
            "[www.imagesplitter.net]-0-3.png");
    Image down1 = new Image("/walk/skeleton_sprites " +
            "[www.imagesplitter.net]-2-8.png");
    Image downAlt1 = new Image("/walk/skeleton_sprites " +
            "[www.imagesplitter.net]-2-5.png");
    Image attackLeft1 = new Image("/monsterAtt/skeleton_sprites " +
            "[www.imagesplitter.net]-5-5.png");
    Image attackLeftAlt1 = new Image("/monsterAtt/skeleton_sprites " +
            "[www.imagesplitter.net]-5-3.png");
    Image attackRightAlt1 = new Image("/monsterAtt/skeleton_sprites " +
            "[www.imagesplitter.net]-7-3.png");
    Image attackRight1 = new Image("/monsterAtt/skeleton_sprites " +
            "[www.imagesplitter.net]-7-5.png");
    Image attackUpAlt1 = new Image("/monsterAtt/skeleton_sprites " +
            "[www.imagesplitter.net]-4-5.png");
    Image attackUp1 = new Image("/monsterAtt/skeleton_sprites " +
            "[www.imagesplitter.net]-4-4.png");
    Image attackDownAlt1 = new Image("/monsterAtt/skeleton_sprites " +
            "[www.imagesplitter.net]-6-2.png");
    Image attackDown1 = new Image("/monsterAtt/skeleton_sprites " +
            "[www.imagesplitter.net]-6-4.png");
    public Image[] imageList1 = {left1, leftAlt1, right1, rightAlt1, up1,
            upAlt1, down1, downAlt1, attackLeft1, attackLeftAlt1, attackRight1,
            attackRightAlt1, attackUp1, attackUpAlt1, attackDown1,
            attackDownAlt1};

    public Image keyImage = new Image("/smallKey.png");

    public defaultMonster() {

    }
}
