package sample;

import javafx.scene.image.Image;



/**
 * Created by subramaniana on 5/31/15.
 */
//an object which loads all our images for the human sprite.
//So instead of loading the images 100 times, we do it once
public class defaultHuman {
    Image left = new Image("/walk/male_sprites " +
            "[www.imagesplitter.net]-9-4.png");
    Image leftAlt = new Image("/walk/male_sprites " +
            "[www.imagesplitter.net]-9-2.png");
    Image right = new Image("/walk/male_sprites " +
            "[www.imagesplitter.net]-11-5.png");
    Image rightAlt = new Image("/walk/male_sprites " +
            "[www.imagesplitter.net]-11-0.png");
    Image up = new Image("/walk/male_sprites " +
            "[www.imagesplitter.net]-8-1.png");
    Image upAlt = new Image("/walk/male_sprites " +
            "[www.imagesplitter.net]-8-3.png");
    Image down = new Image("/walk/male_sprites " +
            "[www.imagesplitter.net]-10-8.png");
    Image downAlt = new Image("/walk/male_sprites " +
            "[www.imagesplitter.net]-10-5.png");
    Image attackLeft = new Image("/humanAtt/male_sprites " +
            "[www.imagesplitter.net]-13-3.png");
    Image attackLeftAlt = new Image("/humanAtt/male_sprites " +
            "[www.imagesplitter.net]-13-5.png");
    Image attackRightAlt = new Image("/humanAtt/male_sprites " +
            "[www.imagesplitter.net]-15-5.png");
    Image attackRight = new Image("/humanAtt/male_sprites " +
            "[www.imagesplitter.net]-15-3.png");
    Image attackUpAlt = new Image("/humanAtt/male_sprites " +
            "[www.imagesplitter.net]-12-5.png");
    Image attackUp = new Image("/humanAtt/male_sprites " +
            "[www.imagesplitter.net]-12-3.png");
    Image attackDownAlt = new Image("/humanAtt/male_sprites " +
            "[www.imagesplitter.net]-14-5.png");
    Image attackDown = new Image("/humanAtt/male_sprites " +
            "[www.imagesplitter.net]-14-3.png");
    public Image[] imageList = {left, leftAlt, right, rightAlt, up, upAlt, down,
            downAlt, attackLeft, attackLeftAlt, attackRight, attackRightAlt,
            attackUp, attackUpAlt, attackDown, attackDownAlt};


    public defaultHuman() {}
}
