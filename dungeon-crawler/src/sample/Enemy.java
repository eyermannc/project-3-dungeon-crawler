package sample;

import java.lang.Math;


/**
 * Created by subramaniana on 5/27/15.
 */
public abstract class Enemy extends Creature {
    private int timeTillNotAngry = -1;
    public int type; //0 = goblin, 1 = skeleton

    public Enemy(Model model) {
        super(model);
    }

    //removes this object from list of monsters on a given floor.
    public void die() {
        modelref.currentFloor.removeMonster(this);
        modelref.score += 100;
    }
    //can it see you????
    public boolean canSeeYou() {
        return canISee(modelref.you.getLocation().getX()+0.5,
                modelref.you.getLocation().getY()+0.5);
    }

    public void charge() {//hopefuly into battle
        //move towards you in one direction, if it hits a wall
        //go in the other direction!
        int x2 = (int) modelref.you.getLocation().getX();
        int y2 = (int) modelref.you.getLocation().getY();
        boolean goX = false;
        if (Math.abs(x2 - x) > Math.abs(y2 - y)) {
            goX = true;
        }
        if(goX) {
            if(x > x2) {
                boolean didMove = this.moveLeft();
                if(!didMove) {
                    if(y > y2) {
                        this.moveUp();
                    }
                    else if(y < y2) {
                        this.moveDown();
                    }
                }
            }
            else if(x < x2) {
                boolean didMove = this.moveRight();
                if(!didMove) {
                    if(y > y2) {
                        this.moveUp();
                    }
                    else if(y < y2) {
                        this.moveDown();
                    }
                }
            }
        }
        else {
            if(y > y2) {
                boolean didMove = this.moveUp();
                if(!didMove) {
                    if(x > x2) {
                        this.moveLeft();
                    }
                    else if(x < x2) {
                        this.moveRight();
                    }
                }
            }
            else if(y < y2) {
                boolean didMove = this.moveDown();
                if(!didMove) {
                    if(x > x2) {
                        this.moveLeft();
                    }
                    else if(x < x2) {
                        this.moveRight();
                    }
                }
            }
        }
    }

    int direction = -1;

    private void patrol() {
        double r = Math.random();
        int newDirection= (int) (4*r);
        if(direction == -1) {
            direction = newDirection;
        }
        else {
            if(newDirection%2 == direction%2 && newDirection != direction) {
                direction = -1;
            }
        }
        if(direction == -1) {
            // don't move
        }
        else if(direction == 0) {
            this.moveUp();
        }
        else if(direction == 1) {
            this.moveRight();
        }
        else if(direction == 2) {
            this.moveDown();
        }
        else if(direction == 3) {
            this.moveLeft();
        }
    }

    public void ai() {
        isAttacking = false;
        if(health <= 0) {
            die();
        }
        int x2 = (int) modelref.you.getLocation().getX();
        int y2 = (int) modelref.you.getLocation().getY();
        double distance = Math.sqrt((x-x2)*(x-x2)+(y-y2)*(y-y2));
        if(canSeeYou()) {
            timeTillNotAngry = 10; //get MAD!
        }
        else {
            timeTillNotAngry--;
        }
        if(timeTillNotAngry > 0) {
            if(distance == 1) {
                attack();
                isAttacking = true;
            }
            else {
                charge();
            }
        }
        else {
            patrol();
        }
    }

    private boolean canISee(double endX, double endY) {
        double X = x+0.5;
        double Y = y+0.5;
        double deltaX = endX - X;
        double deltaY = endY - Y;
        if(deltaX == 0) {
            // vertical
            while(true) {
                double oldY = Y;
                if(deltaY > 0) {
                    Y = strongRoundUp(Y);
                }
                else {
                    Y = strongRoundDown(Y);
                }
                int tileX = (int) X;
                int tileY = (int) (oldY+Y)/2;
                int tileValue = modelref.currentFloor.tiles[tileX][tileY];
                if(tileValue == 1 || tileValue == 3 || tileValue == 4 ||
                        tileValue == 5) {
                    return false;
                }
                if(tileX == (int) endX && tileY == (int) endY) {
                    return true;
                }
            }
        }
        else if(deltaY == 0) {
            // horizontal
            while(true) {
                double oldX = X;
                if(deltaX > 0) {
                    X = strongRoundUp(X);
                }
                else {
                    X = strongRoundDown(X);
                }
                int tileX = (int) (oldX+X)/2;
                int tileY = (int) Y;
                int tileValue = modelref.currentFloor.tiles[tileX][tileY];
                if(tileValue == 1 || tileValue == 3 || tileValue == 4 ||
                        tileValue == 5) {
                    return false;
                }
                if(tileX == (int) endX && tileY == (int) endY) {
                    return true;
                }
            }
        }
        else {
            // diagonal
            while(true) {
                double lineX, lineY;
                if(deltaX > 0) {
                    lineX = strongRoundUp(X);
                }
                else {
                    lineX = strongRoundDown(X);
                }
                if(deltaY > 0) {
                    lineY = strongRoundUp(Y);
                }
                else {
                    lineY = strongRoundDown(Y);
                }

                double oldX = X;
                double oldY = Y;

                double changeX, changeY;

                if(Math.abs((lineX-X)/deltaX) < Math.abs((lineY-Y)/deltaY)) {
                    // move to next integer x position
                    changeX = lineX-X;
                    changeY = deltaY/deltaX*(lineX-X);
                }
                else {
                    // move to next integer y position
                    changeX = deltaX/deltaY*(lineY-Y);
                    changeY = lineY-Y;
                }

                X += changeX;
                Y += changeY;

                int tileX = (int) (oldX+X)/2;
                int tileY = (int) (oldY+Y)/2;

                int tileValue = modelref.currentFloor.tiles[tileX][tileY];
                if(tileValue == 1 || tileValue == 3 || tileValue == 4 ||
                        tileValue == 5) {
                    return false;
                }
                if(tileX == (int) endX && tileY == (int) endY) {
                    return true;
                }
            }
        }
    }

    private int strongRoundUp(double x) {
        if(x == (int) x) {
            return (int) (x+1);
        }
        else {
            return (int) Math.ceil(x);
        }
    }

    private int strongRoundDown(double x) {
        if(x == (int) x) {
            return (int) (x-1);
        }
        else {
            return (int) Math.floor(x);
        }
    }

    public void attack() {
        int deltaX = modelref.you.x - x;
        int deltaY = modelref.you.y - y;
        if(deltaY < 0) {
            orientation = 0;
        }
        else if(deltaX > 0) {
            orientation = 1;
        }
        else if(deltaY > 0) {
            orientation = 2;
        }
        else if(deltaX < 0) {
            orientation = 3;
        }


        if(orientation == 0) {
            modelref.currentFloor.hitboxes[x][y-1] += 10;
        }
        else if(orientation == 1) {
            modelref.currentFloor.hitboxes[x+1][y] += 10;
        }
        else if(orientation == 2) {
            modelref.currentFloor.hitboxes[x][y+1] += 10;
        }
        else if(orientation == 3) {
            modelref.currentFloor.hitboxes[x-1][y] += 10;
        }
    }

}
