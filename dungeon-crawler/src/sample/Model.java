package sample;

/**
 * Created by thomasredding on 5/24/15.
 */

import javafx.geometry.Point2D;

import java.util.List;
import java.util.ArrayList;

public class Model {
    public int windowWidth = 0;
    public int windowHeight = 0;
    InputState inputState;
    Human you = new Human(this);
    private InputState input;
    public List<Floor> floors;
    public int currentFloorIndex;
    public Floor currentFloor;
    public void setInput(InputState inputRef) {
        input = inputRef;
    }
    FloorFactory floorFactory;
    int score = 0;
    int frameNum = 0;
    int floorNum = -1;


    public void loadNewLevel() {
        floorNum++;
        floors.add(floorFactory.makeFloor(64, 64));
        currentFloorIndex = floors.size()-1;
        currentFloor = floors.get(currentFloorIndex);
        you.x = 3;
        you.y = 3;
        you.health = 100;

        double monsterGenerationOdds = (1.0+1.0*floors.size())/100.0;
        for(int i=0; i<currentFloor.tiles.length; i++) {
            for(int j=0; j<currentFloor.tiles[i].length; j++) {
                if(i > 9 || j > 9) { // stop spawning in starting room
                    if(currentFloor.tiles[i][j] == 0) {
                        double whichMonster = Math.random();
                        if(Math.random() < monsterGenerationOdds) {
                            if(whichMonster > .5) {
                                Goblin goblinToAdd = new Goblin(this);
                                goblinToAdd.x = i;
                                goblinToAdd.y = j;
                                currentFloor.monsters.add(goblinToAdd);
                            } else {
                                Skeleton skeletonToAdd = new Skeleton(this);
                                skeletonToAdd.x = i;
                                skeletonToAdd.y = j;
                                currentFloor.monsters.add(skeletonToAdd);
                            }
                        }
                    }
                }
            }
        }
    }

    public Model() {
        floors = new ArrayList<Floor>();
        floorFactory = new FloorFactory(this);
    }

    /*
     * think() is called periodically (roughly 10 times per second). It passes
     * input information onto "you" and loops through all the enemies' think()
     * functions.
     */
    public void think() {
        // run monster AIs
        currentFloor.clearHitBoxes();

        for (int i=0; i<currentFloor.monsters.size(); i++) {
            currentFloor.monsters.get(i).update();
            currentFloor.monsters.get(i).ai();
        }

        you.isAttacking = false;
        you.handleInput(input);

        currentFloor.dealDamage();

        if(currentFloor.hitboxes[you.x][you.y] > 0) {
            you.timeSinceLastDamaged = 0;
        }
        you.health -= currentFloor.hitboxes[you.x][you.y];

        you.think();

        if(frameNum%2 == 0) {
            score--;
        }
        if(score < 0) {
            score = 0;
        }
        frameNum++;
    }
}
