package sample;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by thomasredding on 6/8/15.
 */
public class CreatureTest {

    SimpleModel model = new SimpleModel();

    // These 8 tests evaluate each of the 4 directions twice.
    // The first test considers a successful move in the given direction.
    // The second test considers an unsuccessful move in the given direction
    // (because it was blocked by a wall).
    // The 9th test makes sure that creatures can't move through each other.

    @Test
    public void testDoesMoveDown() throws Exception {
        Enemy enemy = model.currentFloor.monsters.get(0);
        enemy.x = 12;
        enemy.y = 3;
        boolean isSuccessful = enemy.moveDown();
        assertEquals(true, isSuccessful);
        assertEquals(12, enemy.x);
        assertEquals(4, enemy.y);
    }

    @Test
    public void testDoesNotMoveDown() throws Exception {
        Enemy enemy = model.currentFloor.monsters.get(0);
        enemy.x = 12;
        enemy.y = 8;
        boolean isSuccessful = enemy.moveDown();
        assertEquals(false, isSuccessful);
        assertEquals(12, enemy.x);
        assertEquals(8, enemy.y);
    }

    @Test
    public void testDoesMoveUp() throws Exception {
        Enemy enemy = model.currentFloor.monsters.get(0);
        enemy.x = 12;
        enemy.y = 3;
        boolean isSuccessful = enemy.moveUp();
        assertEquals(true, isSuccessful);
        assertEquals(12, enemy.x);
        assertEquals(2, enemy.y);
    }

    @Test
    public void testDoesNotMoveUp() throws Exception {
        Enemy enemy = model.currentFloor.monsters.get(0);
        enemy.x = 12;
        enemy.y = 1;
        boolean isSuccessful = enemy.moveUp();
        assertEquals(false, isSuccessful);
        assertEquals(12, enemy.x);
        assertEquals(1, enemy.y);
    }

    @Test
    public void testDoesMoveRight() throws Exception {
        Enemy enemy = model.currentFloor.monsters.get(0);
        enemy.x = 12;
        enemy.y = 3;
        boolean isSuccessful = enemy.moveRight();
        assertEquals(true, isSuccessful);
        assertEquals(13, enemy.x);
        assertEquals(3, enemy.y);
    }

    @Test
    public void testDoesNotMoveRight() throws Exception {
        Enemy enemy = model.currentFloor.monsters.get(0);
        enemy.x = 15;
        enemy.y = 3;
        boolean isSuccessful = enemy.moveRight();
        assertEquals(false, isSuccessful);
        assertEquals(15, enemy.x);
        assertEquals(3, enemy.y);
    }

    @Test
    public void testDoesMoveLeft() throws Exception {
        Enemy enemy = model.currentFloor.monsters.get(0);
        enemy.x = 12;
        enemy.y = 3;
        boolean isSuccessful = enemy.moveLeft();
        assertEquals(true, isSuccessful);
        assertEquals(11, enemy.x);
        assertEquals(3, enemy.y);
    }

    @Test
    public void testDoesNotMoveLeft() throws Exception {
        Enemy enemy = model.currentFloor.monsters.get(0);
        enemy.x = 10;
        enemy.y = 3;
        boolean isSuccessful = enemy.moveLeft();
        assertEquals(false, isSuccessful);
        assertEquals(10, enemy.x);
        assertEquals(3, enemy.y);
    }

    @Test
    public void testDoesNotMoveThroughCreature() throws Exception {
        model.currentFloor.monsters.add(new Goblin(model));
        Enemy enemyA = model.currentFloor.monsters.get(0);
        Enemy enemyB = model.currentFloor.monsters.get(1);
        enemyA.x = 12;
        enemyA.y = 3;
        enemyB.x = 13;
        enemyB.y = 3;
        boolean isSuccessful = enemyA.moveRight();
        assertEquals(false, isSuccessful);
        assertEquals(12, enemyA.x);
        assertEquals(3, enemyA.y);
        assertEquals(13, enemyB.x);
        assertEquals(3, enemyB.y);
    }
}