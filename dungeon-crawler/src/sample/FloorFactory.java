package sample;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thomasredding on 5/26/15.
 */

/*
 * Nothing in this class is tested because every method/class in FloorFactory
 * is either (1) trivial or (2) the goal is more complicated to prove true than
 * it is to simply achieve.
 */

public class FloorFactory {
    public FloorFactory(Model modelRef) {
        model = modelRef;
    }
    public Model model;
    public  Floor floor;
    private List<Room> rooms;
    private List<Door> mainDoors;

    /*
         * 0 - empty
         * 1 - wall
         * 2 - open door
         * 3 - closed door
         * 4 - locked door
         * 5 - wall adjacent to room
         * 6 - level-winning square
         */

    /*
     * Randomly generates a new floor
     * @param width width of the floor to be created
     * @param height height of the floor to be created
     * @return the newly created floor
     */
    public Floor makeFloor(int width, int height) {
        rooms = new ArrayList<Room>();
        floor = new Floor(model);
        mainDoors = new ArrayList<Door>();
        floor.tiles =  new int[width][height];
        floor.hitboxes = new int[width][height];

        // fill floor with wall tiles
        for(int x=0; x<width; x++) {
            for(int y=0; y<height; y++) {
                floor.tiles[x][y] = 1;
            }
        }

        // create the initial room in the upper-left corner
        rooms.add(new Room(1, 1, 8, 8));
        for(int x=0; x<=9; x++) {
            for(int y=0; y<=9; y++) {
                floor.tiles[x][y] = 5;
            }
        }
        for(int x=1; x<9; x++) {
            for(int y=1; y<9; y++) {
                floor.tiles[x][y] = 0;
            }
        }

        // create "main path" - a path of up to 6 rooms, the initial room, and
        // the final room
        for(int i=0; i<7; i++) {
            int counter = 0;
            while(counter < 100) {
                // keep trying to attach room until you
                // succeed or you have failed 100 times
                if(attemptToAttachRoom(rooms.get(i), true)) {
                    break;
                }
                counter++;
            }
            if(counter == 100) {
                break;
            }
        }

        // create the final (winning) room
        Room room = rooms.get(rooms.size()-1);
        for(int i=room.x; i<room.x+room.width; i++) {
            for(int j=room.y; j<room.y+room.height; j++) {
                floor.tiles[i][j] = 6;
            }
        }

        int mainPathLength = rooms.size()-1;
        int[] numberOfSideRooms = new int[mainPathLength];
        for(int i=0; i<numberOfSideRooms.length; i++) {
            numberOfSideRooms[i] = 0;
        }

        // create up to 5 side rooms that branch off from the main rooms
        for(int i=0; i<5; i++) {
            int counter = 0;
            // keep trying to attach a room until you succeed or
            // until you have failed 100 times
            while(counter < 100) {
                int r = (int) (mainPathLength*Math.random());
                boolean didSucceed = attemptToAttachRoom(rooms.get(r), false);
                if(didSucceed) {
                    numberOfSideRooms[r]++;
                    if(numberOfSideRooms[r] == 1) {
                        // lock the door on the main path
                        floor.tiles[mainDoors.get(r).x][mainDoors.get(r).y] = 4;

                        // put key in the new side room;
                        putKeyInRoom(rooms.get(rooms.size() - 1));
                    }
                    else {
                        // the main room "r" already has a side room with a key
                        // therefore, this newly added side room
                        // doesn't need a key
                    }
                    break;
                }
                counter++;
            }
            if(counter == 100) {
                // We've tried to attach random side rooms to
                // random main rooms 100 times, each of which has failed.
                // We are giving up and simply returning the floor.
            }
        }
        return floor;
    }

    /*
     * puts a key in a random square inside the inputed room
     * @param room room in which to insert a new key
     */
    private void putKeyInRoom(Room room) {
        int x = (int) (room.x + Math.random()*room.width);
        int y = (int) (room.y + Math.random()*room.height);
        floor.items.add(new Key(x, y));
    }

    /*
     * Attempts to create a new room. If mainPath is true,
     * then it trys to create a new room attached to the last room in the rooms
     * list. Otherwise, it randomly selects a room to which to attach the newly
     * created room. If this process fails for any reason, it returns false and
     * leaves the floor unaltered.
     * @param from
     * @param mainPath boolean indicating whether or not
     * the room to add is considered part of the "main path"
     * @return indicates whether this attempt to create a room was successful
     */
    private boolean attemptToAttachRoom(Room from, boolean mainPath) {
        double r = Math.random();
        if(mainPath && (r < 0.25 || r > 0.75)) {
            r = Math.random();
        }
        Room room = new Room(0, 0, (int) (8*Math.random()+5),
                (int) (8*Math.random()+5));
        int doorX, doorY;


        if(r < 0.25) {
            // consider adding a room above
            room.x = (int) ((from.width+room.width-1)*Math.random() +
                    from.x+1-room.width);
            room.y = from.y-room.height-1;
            Range xRange = findOverlap(room.x, room.x+room.width, from.x,
                    from.x+from.width);
            doorX = (int) ((xRange.max-xRange.min)*Math.random() + xRange.min);
            doorY = from.y-1;
        }
        else if(r < 0.50) {
            // consider adding a room to the right
            room.x = from.x+from.width+1;
            room.y = (int) ((from.height+room.height-1)*Math.random() +
                    from.y+1-room.height);
            doorX = room.x-1;
            Range yRange = findOverlap(room.y, room.y+room.height, from.y,
                    from.y+from.height);
            doorY = (int) ((yRange.max-yRange.min)*Math.random() + yRange.min);
        }
        else if(r < 0.75) {
            // consider adding a room below
            room.x = (int) ((from.width+room.width-1)*Math.random() +
                    from.x+1-room.width);
            room.y = from.y+from.height+1;
            Range xRange = findOverlap(room.x, room.x+room.width, from.x,
                    from.x+from.width);
            doorX = (int) ((xRange.max-xRange.min)*Math.random() + xRange.min);
            doorY = room.y-1;
        }
        else {
            // consider adding a room to the left
            room.x = from.x-room.width-1;
            room.y = (int) ((from.height+room.height-1)*Math.random() +
                    from.y+1-room.height);
            doorX = from.x-1;
            Range yRange = findOverlap(room.y, room.y+room.height, from.y,
                    from.y+from.height);
            doorY = (int) ((yRange.max-yRange.min)*Math.random() + yRange.min);
        }

        // If the room would have area off the grid, abort its creation
        if(room.x <= 0 || room.x+room.width >= floor.tiles.length-1) {
            return false;
        }
        if(room.y <= 0 || room.y+room.height >= floor.tiles[0].length-1) {
            return false;
        }

        // If the area where the new room would be contains anything
        // that isn't wall, then abort the creation process.
        for(int i=room.x-1; i<room.x+room.width+1; i++) {
            for(int j=room.y-1; j<room.y+room.height+1; j++) {
                if(floor.tiles[i][j] != 1 && floor.tiles[i][j] != 5) {
                    return false;
                }
            }
        }

        // Convert the tiles corresponding to the room's location
        // from wall (1 or 5) to empty (0)
        for(int i=room.x; i<room.x+room.width; i++) {
            for(int j=room.y; j<room.y+room.height; j++) {
                floor.tiles[i][j] = 0;
            }
        }

        // If this room should be added to the main path,
        // then add the door that leads into the new room to the list of doors
        if(mainPath) {
            mainDoors.add(new Door(doorX, doorY));
        }

        // add the door
        floor.tiles[doorX][doorY] = 3;

        // surround the room with the special "5" wall tile
        for(int i=-1; i<room.width+1; i++) {
            if(floor.tiles[room.x+i][room.y-1] == 1) {
                floor.tiles[room.x+i][room.y-1] = 5;
            }
            if(floor.tiles[room.x+i][room.y+room.height] == 1) {
                floor.tiles[room.x+i][room.y+room.height] = 5;
            }
        }
        for(int i=0; i<room.height; i++) {
            if(floor.tiles[room.x-1][room.y+i] == 1) {
                floor.tiles[room.x-1][room.y+i] = 5;
            }
            if(floor.tiles[room.x+room.width][room.y+i] == 1) {
                floor.tiles[room.x+room.width][room.y+i] = 5;
            }
        }

        // add the room to the official list of rooms
        rooms.add(room);
        return true;
    }

    private Range findOverlap(int minA, int maxA, int minB, int maxB) {
        if(maxA > maxB && minA < minB) {
            // A is superset of B
            return new Range(minB, maxB);
        }
        else if(maxA < maxB && minA > minB) {
            // A is subset of B
            return new Range(minA, maxA);
        }
        else if(minA < minB && maxA < minB) {
            // A is strictly smaller than B
            return new Range(-1, -1);
        }
        else if(minB < minA && maxB < minA) {
            // A is strictly larger than B
            return new Range(0, 0);
        }
        else {
            return new Range(Math.max(minA, minB), Math.min(maxA, maxB));
        }
    }

    private class Range {
        public int min;
        public int max;
        public Range(int a, int b) {
            min = a;
            max = b;
        }
    }

    private class Door {
        int x;
        int y;
        public Door(int X, int Y) {
            x = X;
            y = Y;
        }
    }

    private class Room {
        int x;
        int y;
        int width;
        int height;

        public Room(int X, int Y, int W, int H) {
            x = X;
            y = Y;
            width = W;
            height = H;
        }
    }
}
