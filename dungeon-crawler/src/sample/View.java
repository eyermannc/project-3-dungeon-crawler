package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thomasredding on 5/24/15.
 */

public class View {

    private Model model;
    private Canvas cvs;
    private GraphicsContext c;
    private List<AnimationMonster> listOfMonsters = new ArrayList<AnimationMonster>();
    int cameraX;
    int cameraY;
    int scale = 48; // pixels per tile
    int frameNum = 0;
    defaultMonster def = new defaultMonster();
    defaultHuman defme = new defaultHuman();
    private AnimationHuman me;

    private void updateMonsters(){
        updateListOfMonsters();
        for(int i = 0; i < listOfMonsters.size(); i++){
            listOfMonsters.get(i).update(frameNum);
        }
    }

    public View(Canvas canvas) {
        cvs = canvas;
        c = cvs.getGraphicsContext2D();
    }

    public void setModel(Model modelRef) {
        model = modelRef;
        updateListOfMonsters();
        me = new AnimationHuman(model.you, defme.imageList);
    }

    private void updateListOfMonsters() {
        listOfMonsters = new ArrayList<AnimationMonster>();
        Enemy monster;
        Image[] imgList;
        for(int i=0; i<model.currentFloor.monsters.size(); i++) {
            monster = model.currentFloor.monsters.get(i);
            //checks monster to see what type it is,
            // decides spriteset based on that.
            if(monster.type == 0){
                imgList = def.imageList;
            } else {
                imgList = def.imageList1;
            }

            listOfMonsters.add(new AnimationMonster(
                    monster, imgList));
        }
    }

    public void draw() {
        this.updateMonsters();
        me.update();
        c.clearRect(0, 0, 100000, 100000);
        int tilesWide = ((int) c.getCanvas().getWidth())/scale;
        int tilesHigh = ((int) c.getCanvas().getHeight())/scale;
        cameraX = model.you.x - tilesWide/2;
        cameraY = model.you.y - tilesHigh/2;
        if(cameraX < 0) {
            cameraX = 0;
        }
        if(cameraX + tilesWide > model.currentFloor.tiles.length) {
            cameraX = model.currentFloor.tiles.length - tilesWide;
        }
        if(cameraY < 0) {
            cameraY = 0;
        }
        if(cameraY + tilesHigh >= model.currentFloor.tiles[0].length) {
            cameraY = model.currentFloor.tiles[0].length - tilesHigh;
        }

        c.clearRect(0, 0, 100000, 100000);

        /*
         * 0 - empty
         * 1 - wall
         * 2 - open door
         * 3 - closed door
         * 4 - locked door
         */
        // draw tiles

        Image wall = new Image("/Wall.png");
        Image floor = new Image("/AncientFloor.png");
        Image closed_door = new Image("/DungeonDoor-Closed.png");
        Image open_door = new Image("/DungeonDoor-Open.png");
        Image locked_door = new Image("MysticalDoor-Closed.PNG");

        for(int x=cameraX; x<Math.ceil(cameraX+tilesWide+1); x++) {
            for(int y=cameraY; y<Math.ceil(cameraY+tilesHigh+1); y++) {
                if(x >= 0 && x < model.currentFloor.tiles.length) {
                    if(y >= 0 && y < model.currentFloor.tiles[x].length) {
                        // (x,y) refers to a valid tile
                        if(model.currentFloor.tiles[x][y] == 0) {
                            //floor tile
                            c.drawImage(floor, modelXToScreenX(x),
                                    modelYToScreenY(y), scale, scale);
                        }
                        else if(model.currentFloor.tiles[x][y] == 1) {
                            //background fill
                            c.setFill(Color.BLACK);
                            c.fillRect(modelXToScreenX(x), modelYToScreenY(y),
                                    scale, scale);
                        }
                        else if(model.currentFloor.tiles[x][y] == 2) {
                            // open door
                            c.drawImage(open_door, modelXToScreenX(x),
                                    modelYToScreenY(y), scale, scale);
                        }
                        else if(model.currentFloor.tiles[x][y] == 3) {
                            // closed door
                            c.drawImage(closed_door, modelXToScreenX(x),
                                    modelYToScreenY(y), scale, scale);
                        }
                        else if(model.currentFloor.tiles[x][y] == 4) {
                            // locked door
                            c.drawImage(locked_door, modelXToScreenX(x),
                                    modelYToScreenY(y), scale, scale);
                        }
                        else if(model.currentFloor.tiles[x][y] == 5) {
                            // wall
                            c.drawImage(wall, modelXToScreenX(x),
                                    modelYToScreenY(y), scale, scale);
                        }
                        else if(model.currentFloor.tiles[x][y] == 6) {
                            // winning squares
                            c.setFill(Color.GOLD);
                            c.fillRect(modelXToScreenX(x),
                                    modelYToScreenY(y), scale, scale);
                        }
                    }
                }
            }
        }

        // draw you
        if(model.you.isAttacking) {
            c.drawImage(me.attack(), modelXToScreenX(model.you.x),
                    modelYToScreenY(model.you.y), scale, scale);
        }
        else {
            c.drawImage(me.currentImage, modelXToScreenX(model.you.x),
                    modelYToScreenY(model.you.y), scale, scale);
        }

        // draw monsters' health bar and draw monster
        c.setFill(Color.RED);
        c.setStroke(Color.RED);
        for (int i=0; i < listOfMonsters.size(); i++) {
            AnimationMonster monster = listOfMonsters.get(i);
            Image image = listOfMonsters.get(i).currentImage;
            if(monster.getMonster().isAttacking) {
                image = listOfMonsters.get(i).attack();
            }
            c.drawImage(image, modelXToScreenX(monster.x),
                    modelYToScreenY(monster.y), scale, scale);
            c.fillRect(modelXToScreenX(monster.x), modelYToScreenY(monster.y) -
                    scale / 8, scale * monster.getMonster().health /
                    (monster.getMonster().maxHealth * 1.0), scale / 8);
            c.strokeRect(modelXToScreenX(monster.x), modelYToScreenY(monster.y)
                    - scale / 8, scale, scale / 8);

        }

        // draw keys
        for(int i = 0; i < model.currentFloor.items.size(); i++){
            if (model.currentFloor.items.get(i).type.equals("Key")){
                Item key = model.currentFloor.items.get(i);
                c.drawImage(def.keyImage, modelXToScreenX(key.getX()),
                        modelYToScreenY(key.getY()), scale, scale);
            }
        }

        drawGUI();
        frameNum++;
    }

    private void drawGUI() {
        c.setFill(Color.GREEN);
        c.setStroke(Color.RED);
        if(model.you.health <= 30) {
            // low health -> blink
            if(frameNum%6 < 3) {
                c.fillRect(680, 10, model.you.health, 20);
            }
        }
        else {
            c.fillRect(680, 10, model.you.health, 20);
        }
        c.strokeRect(680, 10, 100, 20);
        c.fillText(model.score + "", 220, 50);
        c.drawImage(def.keyImage, 350, 10);

        c.fillText("x" + model.you.items.size(), 380, 50);
        c.setFill(Color.CORNFLOWERBLUE);
        c.fillText("floor: " + model.floorNum, 10, 50);
    }

    private double modelXToScreenX(double x) {
        return (x-cameraX)*scale;
    }

    private double modelYToScreenY(double y) {
        return (y-cameraY)*scale;
    }

}
