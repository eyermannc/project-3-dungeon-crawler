package sample;

/**
 * Created by thomasredding on 5/24/15.
 */

import javafx.geometry.Point2D;

import java.util.List;
import java.util.ArrayList;

public class SimpleModel extends Model {
    InputState inputState;
    Human you = new Human(this);
    private InputState input;
    private SimpleFloorFactory floorFactory;
    int score = 0;
    int frameNum = 0;


    public void loadNewLevel() {
        floorFactory.makeFloor(64, 64);
        Floor f = floorFactory.makeFloor(64, 64);
        floors.add(f);
        currentFloorIndex = 0;
        currentFloor = floors.get(currentFloorIndex);
        you.x = 3;
        you.y = 3;
        you.health = 100;
        Goblin goblinToAdd = new Goblin(this);
        goblinToAdd.x = 13;
        goblinToAdd.y = 8;
        currentFloor.monsters.add(goblinToAdd);
    }

    public SimpleModel() {
        floors = new ArrayList<Floor>();
        floorFactory = new SimpleFloorFactory(this);
        loadNewLevel();
    }

    /*
     * think() is called periodically (roughly 10 times per second). It passes
     * input information onto "you" and loops through all the enemies's think()
     * functions.
     */
    public void think() {
        // run monster AIs
        currentFloor.clearHitBoxes();

        for (int i=0; i<currentFloor.monsters.size(); i++) {
            currentFloor.monsters.get(i).update();
            currentFloor.monsters.get(i).ai();
        }

        you.isAttacking = false;
        you.handleInput(input);

        currentFloor.dealDamage();

        if(currentFloor.hitboxes[you.x][you.y] > 0) {
            you.timeSinceLastDamaged = 0;
        }
        you.health -= currentFloor.hitboxes[you.x][you.y];

        you.think();

        if(frameNum%2 == 0) {
            score--;
        }
        if(score < 0) {
            score = 0;
        }
        frameNum++;
    }
}
