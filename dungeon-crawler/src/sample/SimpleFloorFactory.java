package sample;

//import java.awt.geom.Point2D;
//import java.sql.SQLType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thomasredding on 5/26/15.
 */

/*
 * Nothing in this class is tested because every method/class in FloorFactory
 * is either (1) trivial or (2) the goal is more complicated to prove true than
 * it is to simply achieve.
 */

public class SimpleFloorFactory extends FloorFactory {
    public SimpleFloorFactory(Model modelRef) {
        super(modelRef);
    }

    /*
     * Generates a new simple floor
     * @param width width of the floor to be created
     * @param height height of the floor to be created
     * @return the newly created floor
     */
    public Floor makeFloor(int width, int height) {
        floor = new Floor(model);
        floor.tiles =  new int[width][height];
        floor.hitboxes = new int[width][height];

        // fill floor with wall tiles
        for(int x=0; x<width; x++) {
            for(int y=0; y<height; y++) {
                floor.tiles[x][y] = 1;
            }
        }

        // create the initial room in the upper-left corner
        for(int x=0; x<=9; x++) {
            for(int y=0; y<=9; y++) {
                floor.tiles[x][y] = 5;
            }
        }
        for(int x=1; x<1+8; x++) {
            for(int y=1; y<1+8; y++) {
                floor.tiles[x][y] = 0;
            }
        }

        // ad single side room
        for(int x=9; x<=16; x++) {
            for(int y=0; y<=9; y++) {
                floor.tiles[x][y] = 5;
            }
        }
        for(int x=10; x<16; x++) {
            for(int y=1; y<1+8; y++) {
                floor.tiles[x][y] = 0;
            }
        }

        floor.tiles[9][5] = 5;
        return floor;
    }
}
