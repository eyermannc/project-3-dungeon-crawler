package sample;

import javafx.scene.image.Image;

/**
 * Created by thomasredding on 6/1/15.
 */
/*In order to modularize our game,
our view doensn't have a human object.
 Instead, it has an "animation human", which has a reference
 to our actual human object, but also has the sprites we need.
  */
public class AnimationHuman {
    private Human human;

    public Image currentImage;
    int frame = 0;
    public double x = 0;
    public double y = 0;
    private Image leftImages[] = new Image[2];
    private Image rightImages[] = new Image[2];
    private Image upImages[] = new Image[2];
    private Image downImages[] = new Image[2];
    private Image spriteAttackLeft[] = new Image[2];
    private Image spriteAttackRight[] = new Image[2];
    private Image spriteAttackUp[] = new Image[2];
    private Image spriteAttackDown[] = new Image[2];


    public void update() {
        //changes image to correct direction facing
        if(human.getOrientation() == 1) {
            this.currentImage = rightImages[frame%2];
        }
        if(human.getOrientation() == 3) {
            this.currentImage = leftImages[frame%2];
        }
        if(human.getOrientation() == 2) {
            this.currentImage = downImages[frame%2];
        }
        if(human.getOrientation() == 0) {
            this.currentImage = upImages[frame%2];
        }
        if(frame%2 == 0) {
            x = human.x;
            y = human.y;
        }
        else {
            x = (human.x + 0.0 + human.oldX)/2;
            y = (human.y + 0.0 + human.oldY)/2;
        }
        frame++;
    }


    public Image attack() { //returns sprite image of attacking animation
        Image[][] attackSprites = {spriteAttackUp, spriteAttackRight,
                spriteAttackDown, spriteAttackLeft};
        return attackSprites[human.getOrientation()][frame%2];
    }

    public AnimationHuman(Human you, Image[] sprites) {
        leftImages[0] = sprites[0];
        leftImages[1] = sprites[1];
        rightImages[0] = sprites[2];
        rightImages[1] = sprites[3];
        upImages[0] = sprites[4];
        upImages[1] = sprites[5];
        downImages[0] = sprites[6];
        downImages[1] = sprites[7];
        spriteAttackLeft[0] = sprites[8];
        spriteAttackLeft[1] = sprites[9];
        spriteAttackRight[0] = sprites[10];
        spriteAttackRight[1] = sprites[11];
        spriteAttackUp[0] = sprites[12];
        spriteAttackUp[1] = sprites[13];
        spriteAttackDown[0] = sprites[14];
        spriteAttackDown[1] = sprites[15];
        this.human = you;
    }
}
