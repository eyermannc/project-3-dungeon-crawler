package sample;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by thomasredding on 6/7/15.
 */
public class EnemyTest {
    SimpleModel model = new SimpleModel();

    @Test
    public void testDie() throws Exception {
        Creature monster = model.currentFloor.monsters.get(0);
        monster.die();
        assertEquals(0, model.currentFloor.monsters.size());
    }

    @Test
    public void testCanNotSeeYouHorizontally() throws Exception {
        Enemy enemy = model.currentFloor.monsters.get(0);
        enemy.x = 12;
        enemy.y = 3;
        boolean result = enemy.canSeeYou();
        assertEquals(false, result);
    }

    @Test
    public void testCanSeeYouHorizontally() throws Exception {
        Enemy enemy = model.currentFloor.monsters.get(0);
        enemy.x = 5;
        enemy.y = 3;
        boolean result = enemy.canSeeYou();
        assertEquals(true, result);
    }

    @Test
    public void testCanNotSeeYouDiagonally() throws Exception {
        Enemy enemy = model.currentFloor.monsters.get(0);
        enemy.x = 12;
        enemy.y = 5;
        boolean result = enemy.canSeeYou();
        assertEquals(false, result);
    }

    @Test
    public void testCanSeeYouDiagonally() throws Exception {
        Enemy enemy = model.currentFloor.monsters.get(0);
        enemy.x = 5;
        enemy.y = 7;
        boolean result = enemy.canSeeYou();
        assertEquals(true, result);
    }
}
