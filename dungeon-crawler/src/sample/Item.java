package sample;

/**
 * Created by subramaniana on 5/27/15.
 */
public abstract class Item {
    //for keys, potions, and other special items
    //which don't yet exist...
    boolean consumable;
    int xLocation;
    int yLocation;
    boolean possessed;
    public String type;
    public int getX() {
        return this.xLocation;

    }
    public int getY() {
        return this.yLocation;
    }

    public boolean isConsumable() {
        return consumable;
    }

    public boolean isPossessed() {
        return possessed;
    }
}
