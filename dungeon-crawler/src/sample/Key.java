package sample;

/**
 * Created by subramaniana on 5/28/15.
 */
public class Key extends Item {

    Key() {
        super();
        possessed = true;
        type = "Key";
    }

    Key(int x, int y) {
        super();
        possessed = false;
        xLocation = x;
        yLocation = y;
        type = "Key";

    }
}
