package sample;

/**
 * Created by thomasredding on 5/25/15.
 */

import java.util.List;
import java.util.ArrayList;

public class Floor {
    public Floor(Model modelRef) {
        items = new ArrayList<Item>();
        monsters = new ArrayList<Enemy>();
    }

    public List<Enemy> monsters;
    public List<Item> items;

    public void removeMonster(Creature enemyToRemove) {
        monsters.remove(enemyToRemove);
    }
    /*
     * 0 - empty
     * 1 - wall
     * 2 - open door
     * 3 - closed door
     * 4 - locked door
     */
    public int tiles[][];

    public int hitboxes[][];

    public void clearHitBoxes() {
        for(int i=0; i<hitboxes.length; i++) {
            for(int j=0; j<hitboxes.length; j++) {
                hitboxes[i][j] = 0;
            }
        }
    }

    public void dealDamage() {
        for(int i=0; i<monsters.size(); i++) {
            monsters.get(i).health -=
                    hitboxes[monsters.get(i).x][monsters.get(i).y];
        }
    }
}
