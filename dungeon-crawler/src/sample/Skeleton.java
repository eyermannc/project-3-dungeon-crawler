package sample;

/**
 * Created by subramaniana on 6/7/15.
 */
public class Skeleton extends  Enemy {
    public Skeleton(Model model) {
        super(model);
        x = 32;
        y = 34;
        type = 1;
    }
}
